package hackyeah.pl.lotyoung.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.details.presentation.CityPagerAdapter
import hackyeah.pl.lotyoung.details.presentation.TicketViewModel
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity
import kotlinx.android.synthetic.main.activity_ticket_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TicketDetails : AppCompatActivity() {

    private val cityViewModel: TicketViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_details)

        val ticket = intent.getSerializableExtra(INTENT_TICKET) as TicketEntity

        toolbar.title = getString(R.string.flight_to, ticket.toCity)
        setSupportActionBar(toolbar)

        observeCityPhoto()
        setUpPager(ticket)

        val imageUrl: String? = null

        if (imageUrl.isNullOrEmpty()) {

            if (savedInstanceState == null) {
                cityViewModel.fetchCityPhoto(ticket.toCity)
            }
        } else {
            loadCityImage(imageUrl)
        }
    }

    private fun setUpPager(ticket: TicketEntity) {
        pager.adapter = CityPagerAdapter(supportFragmentManager, ticket)
        tabs.setupWithViewPager(pager)
        tabs.removeAllTabs()
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_airplanemode))
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_event))
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_whatshot))
    }

    private fun observeCityPhoto() {
        cityViewModel.cityPhoto().observe(this, Observer {
            it.url?.let { photoUrl -> loadCityImage(photoUrl) }
        })
    }

    private fun loadCityImage(imageUrl: String) {
        Picasso.get().load(imageUrl).fit().centerCrop().into(imageCityBackground)
    }

    companion object {
        private const val INTENT_TICKET = "ticket"

        fun createIntent(
            context: Context,
            ticket: TicketEntity
        ): Intent {
            return Intent(context, TicketDetails::class.java)
                .putExtra(INTENT_TICKET, ticket)
        }
    }
}