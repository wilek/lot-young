package hackyeah.pl.lotyoung.details.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.network.eventbrite.model.Event
import java.text.SimpleDateFormat

internal class EventsAdapter(private val picasso: Picasso) :
    ListAdapter<Event, EventsAdapter.EventViewHolder>(EventsDiff()) {

    private val apiDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    private val uiDateFormat = SimpleDateFormat("yyyy-MM-dd")

    var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false)
        val viewHolder = EventViewHolder(view)
        view.setOnClickListener { onClickListener?.onEventClick(getItem(viewHolder.adapterPosition)) }
        viewHolder.buttnFav.setOnClickListener {
            val item = getItem(viewHolder.adapterPosition)
            item.fav = !item.fav
            notifyItemChanged(viewHolder.adapterPosition)
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val item = getItem(position)

        holder.textEventCity.text = item.name.text
        holder.textEventDate.text = uiDateFormat.format(apiDateFormat.parse(item.start.local))
        item.logo?.url?.let { imageUrl ->
            picasso.load(imageUrl).fit().centerCrop().into(holder.imageEventLogo)
        }

        if (item.fav) {
            holder.buttnFav.setImageResource(R.drawable.ic_star)
        } else {
            holder.buttnFav.setImageResource(R.drawable.ic_star_border)
        }
    }

    internal class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageEventLogo: ImageView = itemView.findViewById(R.id.imageEventLogo)
        val textEventCity: TextView = itemView.findViewById(R.id.textEventName)
        val textEventDate: TextView = itemView.findViewById(R.id.textEventDate)
        val buttnFav: ImageButton = itemView.findViewById(R.id.buttonFav)
    }

    internal class EventsDiff : DiffUtil.ItemCallback<Event>() {
        override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
            return oldItem == newItem
        }
    }

    internal interface OnItemClickListener {
        fun onEventClick(event: Event)
    }
}
