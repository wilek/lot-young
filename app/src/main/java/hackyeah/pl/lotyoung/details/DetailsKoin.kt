package hackyeah.pl.lotyoung.details

import hackyeah.pl.lotyoung.details.data.EventbriteRepository
import hackyeah.pl.lotyoung.details.data.UnsplashRepository
import hackyeah.pl.lotyoung.details.presentation.TicketViewModel
import hackyeah.pl.lotyoung.details.presentation.EventsViewModel
import hackyeah.pl.lotyoung.network.eventbrite.EventbriteNetworkClient
import hackyeah.pl.lotyoung.network.unsplash.UnsplashNetworkClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val detailsKoinModule: Module = module {

    viewModel { TicketViewModel(get()) }

    viewModel { EventsViewModel(get()) }

    factory { UnsplashRepository(get(), get()) }

    single { UnsplashNetworkClient().getClient() }

    single { EventbriteRepository(get(), get()) }

    single { EventbriteNetworkClient().getClient() }
}