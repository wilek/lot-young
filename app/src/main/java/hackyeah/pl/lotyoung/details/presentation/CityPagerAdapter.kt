package hackyeah.pl.lotyoung.details.presentation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity

internal class CityPagerAdapter(
    fm: FragmentManager,
    private val ticket: TicketEntity
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FlightInfoFragment.newInstance(ticket)
            else -> EventsFragment.newInstance(ticket.toCity)
        }
    }

    override fun getCount(): Int {
        return 3
    }
}