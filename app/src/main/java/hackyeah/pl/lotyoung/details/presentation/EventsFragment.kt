package hackyeah.pl.lotyoung.details.presentation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.network.eventbrite.model.Event
import kotlinx.android.synthetic.main.fragment_event.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class EventsFragment : Fragment() {

    private val eventsViewModel: EventsViewModel by viewModel()
    private val adapter = EventsAdapter(Picasso.get())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerEvents.adapter = adapter
        observeEvents()

        val layoutManager = recyclerEvents.layoutManager as LinearLayoutManager
        recyclerEvents.addItemDecoration(DividerItemDecoration(requireContext(), layoutManager.orientation))
        adapter.onClickListener = object: EventsAdapter.OnItemClickListener {
            override fun onEventClick(event: Event) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(event.url)))
            }
        }

        if (savedInstanceState == null) {
            eventsViewModel.searchEvents(arguments?.getString(INTENT_CITY)!!)
        }
    }

    private fun observeEvents() {
        eventsViewModel.events().observe(this, Observer { events ->
            adapter.submitList(events.events)
        })
    }

    companion object {
        private const val INTENT_CITY = "city"

        fun newInstance(city: String): Fragment {
            val args = Bundle()
            args.putString(INTENT_CITY, city)

            return EventsFragment().apply { arguments = args }
        }
    }
}