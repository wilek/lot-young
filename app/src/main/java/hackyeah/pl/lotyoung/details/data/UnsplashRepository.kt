package hackyeah.pl.lotyoung.details.data

import hackyeah.pl.lotyoung.details.data.model.CityPhoto
import hackyeah.pl.lotyoung.network.AndroidSchedulerProvider
import hackyeah.pl.lotyoung.network.unsplash.`interface`.UnsplashApi
import hackyeah.pl.lotyoung.network.unsplash.input.PhotoOrientation
import io.reactivex.Single

internal class UnsplashRepository(
    private val unsplashApi: UnsplashApi,
    private val schedulersProvider: AndroidSchedulerProvider
) {

    fun getCityPhoto(
        cityName: String,
        orientation: PhotoOrientation = PhotoOrientation.PORTRAIT,
        itemsCount: Int = DEFAULT_ITEMS_COUNT
    ): Single<CityPhoto> {
        return unsplashApi.searchPhoto(cityName, orientation.toString().toLowerCase(), itemsCount)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.android())
            .map { photos ->
                CityPhoto(photos.results.maxBy { photo -> photo.likes }?.urls?.regular)
            }
    }

    companion object {
        private const val DEFAULT_ITEMS_COUNT = 10
    }
}