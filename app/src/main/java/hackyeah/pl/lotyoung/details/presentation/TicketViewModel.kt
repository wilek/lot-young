package hackyeah.pl.lotyoung.details.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hackyeah.pl.lotyoung.details.data.UnsplashRepository
import hackyeah.pl.lotyoung.details.data.model.CityPhoto
import io.reactivex.disposables.CompositeDisposable

internal class TicketViewModel(private val unsplashRepository: UnsplashRepository): ViewModel() {

    private val disposables = CompositeDisposable()
    private val cityPhoto = MutableLiveData<CityPhoto>()

    fun cityPhoto(): LiveData<CityPhoto> = cityPhoto

    fun fetchCityPhoto(city: String) {
        unsplashRepository.getCityPhoto(city)
            .subscribe({
                cityPhoto.value = it
            }, {
                Log.d("ERROR", "" + it)
                //TODO: Handle error
            }).let {
                disposables.add(it)
            }
    }

    override fun onCleared() {
        disposables.clear()
    }
}