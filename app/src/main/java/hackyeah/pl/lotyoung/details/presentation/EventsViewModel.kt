package hackyeah.pl.lotyoung.details.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hackyeah.pl.lotyoung.details.data.EventbriteRepository
import hackyeah.pl.lotyoung.network.eventbrite.model.Events
import io.reactivex.disposables.CompositeDisposable

internal class EventsViewModel(private val eventbriteRepository: EventbriteRepository): ViewModel() {

    private val disposables = CompositeDisposable()
    private val events = MutableLiveData<Events>()

    fun events(): LiveData<Events> = events

    fun searchEvents(city: String) {
        eventbriteRepository.searchEvents(city)
            .subscribe({
                events.value = it
            }, {
                Log.d("ERROR", "" + it)
                //TODO: Handle error
            }).let {
                disposables.add(it)
            }
    }

    override fun onCleared() {
        disposables.clear()
    }
}