package hackyeah.pl.lotyoung.details.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity
import kotlinx.android.synthetic.main.fragment_flight_info.*

class FlightInfoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_flight_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ticket = arguments!!.getSerializable(INTENT_TICKET) as TicketEntity
        bindData(ticket)
    }

    private fun bindData(ticket: TicketEntity) {
        textFlightFrom.text = ticket.fromCity + " (" + ticket.fromIata + ")"
        textFlightTo.text = ticket.toCity + " (" + ticket.toIata + ")"
        textFlightDate.text = ticket.date
        textGate.text = ticket.gate
        textSeat.text = ticket.seat
        textBoardingTime.text = ticket.time
    }

    companion object {
        private const val INTENT_TICKET = "ticket"

        fun newInstance(ticket: TicketEntity): Fragment {
            val args = Bundle()
            args.putSerializable(INTENT_TICKET, ticket)

            return FlightInfoFragment().apply { arguments = args }
        }
    }
}