package hackyeah.pl.lotyoung.details.data

import hackyeah.pl.lotyoung.network.AndroidSchedulerProvider
import hackyeah.pl.lotyoung.network.eventbrite.EventbriteApi
import hackyeah.pl.lotyoung.network.eventbrite.model.Events
import io.reactivex.Single

internal class EventbriteRepository(
    private val eventbriteApi: EventbriteApi,
    private val schedulersProvider: AndroidSchedulerProvider
) {

    fun searchEvents(location: String): Single<Events> {
        return eventbriteApi.searchEvents(location, "10km", "venue")
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.android())
    }
}