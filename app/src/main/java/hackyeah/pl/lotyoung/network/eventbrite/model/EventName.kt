package hackyeah.pl.lotyoung.network.eventbrite.model

data class EventName(val text: String)