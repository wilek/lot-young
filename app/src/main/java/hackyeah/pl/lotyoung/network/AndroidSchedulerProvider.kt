package hackyeah.pl.lotyoung.network

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

internal class AndroidSchedulerProvider: SchedulersProvider {

    override fun io(): Scheduler {
        return Schedulers.io()
    }

    override fun android(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}