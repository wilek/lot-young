package hackyeah.pl.lotyoung.network.unsplash

import okhttp3.Interceptor
import okhttp3.Response

internal class ApiVersionInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader(HEADER_NAME, VERSION).build()
        return chain.proceed(request)
    }

    companion object {
        private const val HEADER_NAME = "Accept-Version"
        private const val VERSION = "v1"
    }
}