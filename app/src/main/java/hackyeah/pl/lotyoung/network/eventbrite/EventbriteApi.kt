package hackyeah.pl.lotyoung.network.eventbrite

import hackyeah.pl.lotyoung.network.eventbrite.model.Events
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface EventbriteApi {

    @GET("/v3/events/search")
    fun searchEvents(
        @Query("location.address") location: String,
        @Query("location.within") within: String,
        @Query("expand") expand: String
    ): Single<Events>
}