package hackyeah.pl.lotyoung.network.eventbrite

import okhttp3.Interceptor
import okhttp3.Response

internal class AuthInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader(HEADER_NAME, ACCESS).build()
        return chain.proceed(request)
    }

    companion object {
        private const val HEADER_NAME = "Authorization"
        private const val ACCESS = "Bearer 3DP4FU3RZEFEONPY6MG5"
    }
}