package hackyeah.pl.lotyoung.network.unsplash.model

data class PhotoUrls(val full: String, val regular: String, val thumb: String)