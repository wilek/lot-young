package hackyeah.pl.lotyoung.network.unsplash.`interface`

import hackyeah.pl.lotyoung.network.unsplash.model.PhotosResult
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashApi {

    @GET("/search/photos")
    fun searchPhoto(
        @Query("query") query: String,
        @Query("orientation") orientation: String,
        @Query("per_page") itemsCount: Int
    ): Single<PhotosResult>
}