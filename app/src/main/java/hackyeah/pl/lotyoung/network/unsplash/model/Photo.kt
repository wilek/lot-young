package hackyeah.pl.lotyoung.network.unsplash.model

data class Photo(val likes: Int, val urls: PhotoUrls)