package hackyeah.pl.lotyoung.network.lot

import okhttp3.Interceptor
import okhttp3.Response

internal class ApiKeyInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader(
            HEADER_NAME,
            API_KEY
        ).build()
        return chain.proceed(request)
    }

    companion object {
        private const val HEADER_NAME = "X-Api-Key"
        private const val API_KEY = "9YFNNKS31u9gCFKPetPWdAAjEXnED0B3K18AeYgg"
    }
}