package hackyeah.pl.lotyoung.network.unsplash.model

data class PhotosResult(val results: List<Photo>)