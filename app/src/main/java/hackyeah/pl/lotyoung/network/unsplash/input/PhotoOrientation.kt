package hackyeah.pl.lotyoung.network.unsplash.input

enum class PhotoOrientation {
    LANDSCAPE,
    PORTRAIT,
    SQUARISH
}