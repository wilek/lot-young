package hackyeah.pl.lotyoung.network.unsplash

import com.facebook.stetho.okhttp3.StethoInterceptor
import hackyeah.pl.lotyoung.network.unsplash.`interface`.UnsplashApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

internal class UnsplashNetworkClient {

    fun getClient(): UnsplashApi = client().create(UnsplashApi::class.java)

    private fun client(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_ADDRESS)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .addNetworkInterceptor(StethoInterceptor())
                    .addInterceptor(ApiVersionInterceptor())
                    .addInterceptor(AuthInterceptor())
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                    .connectTimeout(3, TimeUnit.SECONDS)
                    .readTimeout(3, TimeUnit.SECONDS)
                    .build())
            .build()
    }

    companion object {
        private const val API_ADDRESS = "https://api.unsplash.com"
    }
}