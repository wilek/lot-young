package hackyeah.pl.lotyoung.network.unsplash

import okhttp3.Interceptor
import okhttp3.Response

internal class AuthInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader(HEADER_NAME, ACCESS).build()
        return chain.proceed(request)
    }

    companion object {
        private const val HEADER_NAME = "Authorization"
        private const val ACCESS = "Client-ID ce6197d582144aad1392a4dd1a0314448add60cb99fadcc7d14172f87a7899da"
    }
}