package hackyeah.pl.lotyoung.network.lot

import okhttp3.Interceptor
import okhttp3.Response

internal class AuthInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().addHeader(
            HEADER_NAME,
            SECRET_KEY
        ).build()
        return chain.proceed(request)
    }

    companion object {
        private const val HEADER_NAME = "secret_key"
        private const val SECRET_KEY = "2przp49a52"
    }
}