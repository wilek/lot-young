package hackyeah.pl.lotyoung.network

import io.reactivex.Scheduler

interface SchedulersProvider {

    fun io(): Scheduler

    fun android(): Scheduler
}