package hackyeah.pl.lotyoung.network.eventbrite.model

data class Event(val name: EventName, val start: EventDate, val logo: EventLogo?, val url: String, var fav: Boolean = false)