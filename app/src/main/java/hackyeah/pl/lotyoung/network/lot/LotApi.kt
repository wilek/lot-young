package hackyeah.pl.lotyoung.network.lot

import hackyeah.pl.lotyoung.airports.repository.network.model.Airport
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface LotApi {

    @Headers("Content-Type: application/json")
    @GET("/flights-dev/v2/common/airports/get")
    fun getAvailableFlights(): Single<List<Airport>>
}