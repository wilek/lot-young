package hackyeah.pl.lotyoung.ticket.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "AvailableTickets")
data class TicketEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val fromCity: String,
    val toCity: String,
    val fromIata: String,
    val toIata: String,
    val date: String,
    val time: String,
    val gate: String,
    val seat: String
) : Serializable