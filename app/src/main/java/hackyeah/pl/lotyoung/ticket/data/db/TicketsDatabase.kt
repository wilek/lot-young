package hackyeah.pl.lotyoung.ticket.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity

@Database(entities = arrayOf(TicketEntity::class), version = 1)
internal abstract class TicketsDatabase : RoomDatabase() {

    abstract fun ticketsDao(): TicketsDao

    companion object {
        private val DATABASE_NAME = "availableTickets"

        fun getDatabase(context: Context): TicketsDatabase {
            return Room.databaseBuilder(context, TicketsDatabase::class.java, DATABASE_NAME).build()
        }
    }
}