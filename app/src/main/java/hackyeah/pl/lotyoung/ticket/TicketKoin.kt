package hackyeah.pl.lotyoung.ticket

import hackyeah.pl.lotyoung.ticket.data.TicketsDatabaseRepository
import hackyeah.pl.lotyoung.ticket.data.TicketsRepository
import hackyeah.pl.lotyoung.ticket.data.db.TicketsDatabase
import hackyeah.pl.lotyoung.ticket.presentation.TicketViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val ticketKoinModule: Module = module {

    viewModel { TicketViewModel(get(), get(), get()) }

    single { TicketsRepository() }

    single { TicketsDatabaseRepository(get(), get()) }

    single { TicketsDatabase.getDatabase(androidContext()).ticketsDao() }
}