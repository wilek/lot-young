package hackyeah.pl.lotyoung.ticket.data.db

import androidx.room.*
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity
import io.reactivex.Single

@Dao
interface TicketsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(ticket: TicketEntity): Single<Long>

    @Query("SELECT * from AvailableTickets")
    fun getTickets(): Single<List<TicketEntity>>
}