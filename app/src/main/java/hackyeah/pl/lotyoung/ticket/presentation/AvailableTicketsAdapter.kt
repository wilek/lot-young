package hackyeah.pl.lotyoung.ticket.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.ticket.data.model.AvailableTicket

internal class AvailableTicketsAdapter :
    ListAdapter<AvailableTicket, AvailableTicketsAdapter.AvailableTicketViewHolder>(
        AvailableTicketDiff()
    ) {

    var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AvailableTicketViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_available_ticket, parent, false)
        val viewHolder = AvailableTicketViewHolder(view)
        viewHolder.buttonBook.setOnClickListener {
            onClickListener?.onAvailableTicketClick(getItem(viewHolder.adapterPosition))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: AvailableTicketViewHolder, position: Int) {
        val item = getItem(position)

        holder.textDate.text = item.date
        holder.textFrom.text = item.fromCity + " (" + item.fromIata + ")"
        holder.textTo.text = item.toCity + " (" + item.toIata + ")"
        holder.textPrice.text = item.price
    }

    internal class AvailableTicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textDate: TextView = itemView.findViewById(R.id.textDate)
        val textFrom: TextView = itemView.findViewById(R.id.textFrom)
        val textTo: TextView = itemView.findViewById(R.id.textTo)
        val textPrice: TextView = itemView.findViewById(R.id.textPrice)
        val buttonBook: Button = itemView.findViewById(R.id.buttonBook)
    }

    internal class AvailableTicketDiff : DiffUtil.ItemCallback<AvailableTicket>() {
        override fun areItemsTheSame(oldItem: AvailableTicket, newItem: AvailableTicket): Boolean {
            return oldItem.fromIata == newItem.fromIata
        }

        override fun areContentsTheSame(
            oldItem: AvailableTicket,
            newItem: AvailableTicket
        ): Boolean {
            return oldItem == newItem
        }
    }

    internal interface OnItemClickListener {
        fun onAvailableTicketClick(availableTicket: AvailableTicket)
    }
}
