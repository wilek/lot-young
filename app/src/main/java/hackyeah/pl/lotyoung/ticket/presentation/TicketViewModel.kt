package hackyeah.pl.lotyoung.ticket.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hackyeah.pl.lotyoung.details.data.UnsplashRepository
import hackyeah.pl.lotyoung.details.data.model.CityPhoto
import hackyeah.pl.lotyoung.ticket.data.TicketsDatabaseRepository
import hackyeah.pl.lotyoung.ticket.data.TicketsRepository
import hackyeah.pl.lotyoung.ticket.data.model.AvailableTicket
import io.reactivex.disposables.CompositeDisposable

internal class TicketViewModel(
    private val unsplashRepository: UnsplashRepository,
    private val ticketsRepository: TicketsRepository,
    private val databaseRepository: TicketsDatabaseRepository
) : ViewModel() {

    private val disposables = CompositeDisposable()
    private val cityPhoto = MutableLiveData<CityPhoto>()
    private val tickets = MutableLiveData<List<AvailableTicket>>()

    fun cityPhoto(): LiveData<CityPhoto> = cityPhoto

    fun tickets(): LiveData<List<AvailableTicket>> = tickets

    fun fetchCityPhoto(city: String) {
        unsplashRepository.getCityPhoto(city)
            .subscribe({
                cityPhoto.value = it
            }, {
                Log.d("ERROR", "" + it)
                //TODO: Handle error
            }).let {
                disposables.add(it)
            }
    }

    fun fetchTickets(city: String, iata: String) {
        tickets.value = ticketsRepository.getTickets(city, iata)
    }

    fun addTicket(ticket: AvailableTicket) {
        databaseRepository.addTicket(ticket).subscribe({
            // nop
        }, {
            // nop
        }).let {
            disposables.add(it)
        }
    }

    override fun onCleared() {
        disposables.clear()
    }
}