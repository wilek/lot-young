package hackyeah.pl.lotyoung.ticket.data

import hackyeah.pl.lotyoung.network.AndroidSchedulerProvider
import hackyeah.pl.lotyoung.ticket.data.db.TicketsDao
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity
import hackyeah.pl.lotyoung.ticket.data.model.AvailableTicket
import io.reactivex.Single

internal class TicketsDatabaseRepository(
    private val ticketsDao: TicketsDao,
    private val schedulersProvider: AndroidSchedulerProvider
) {

    fun addTicket(ticket: AvailableTicket): Single<Long> {
        return ticketsDao.add(
            TicketEntity(
                0,
                ticket.fromCity,
                ticket.toCity,
                ticket.fromIata,
                ticket.toIata,
                ticket.date,
                ticket.time,
                ticket.gate,
                ticket.seat
            )
        )
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.android())
    }

    fun getTickets(): Single<List<TicketEntity>> {
        return ticketsDao.getTickets()
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.android())
    }

}