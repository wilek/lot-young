package hackyeah.pl.lotyoung.ticket.data

import hackyeah.pl.lotyoung.ticket.data.model.AvailableTicket

internal class TicketsRepository {

    fun getTickets(city: String, iata: String): List<AvailableTicket> {
        return listOf(
            AvailableTicket(
                "Warsaw",
                city,
                "WAW",
                iata,
                "2099,99 zł",
                "12.11.2019",
                "23:30",
                "G23",
                "A15"
            ),
            AvailableTicket(
                "Warsaw",
                city,
                "WAW",
                iata,
                "999,99 zł",
                "10.02.2020",
                "12:00",
                "A10",
                "E32"
            ),
            AvailableTicket(
                "Warsaw",
                city,
                "WAW",
                iata,
                "1799,99 zł",
                "12.04.2020",
                "16:40",
                "J13",
                "C05"
            ),
            AvailableTicket(
                "Warsaw",
                city,
                "WAW",
                iata,
                "399,99 zł",
                "18.07.2020",
                "03:10",
                "A01",
                "A19"
            ),
            AvailableTicket(
                "Warsaw",
                city,
                "WAW",
                iata,
                "800,00 zł",
                "01.09.2020",
                "19:20",
                "B02",
                "F30"
            ),
            AvailableTicket(
                "Warsaw",
                city,
                "WAW",
                iata,
                "1200,00 zł",
                "01.11.2020",
                "07:00",
                "D06",
                "B43"
            )
        )
    }
}