package hackyeah.pl.lotyoung.ticket.data.model

data class AvailableTicket(
    val fromCity: String,
    val toCity: String,
    val fromIata: String,
    val toIata: String,
    val price: String,
    val date: String,
    val time: String,
    val gate: String,
    val seat: String
)