package hackyeah.pl.lotyoung.ticket.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.ticket.data.model.AvailableTicket
import kotlinx.android.synthetic.main.activity_ticket.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TicketActivity : AppCompatActivity() {

    private val ticketViewModel: TicketViewModel by viewModel()
    private val availableTicketsAdapter = AvailableTicketsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket)

        val city = intent.getStringExtra(INTENT_CITY)
        val iata = intent.getStringExtra(INTENT_IATA)
        val imageUrl = intent.getStringExtra(INTENT_IMAGE_URL)

        toolbar.title = city
        setSupportActionBar(toolbar)

        recyclerBuyTickets.adapter = availableTicketsAdapter
        recyclerBuyTickets.addItemDecoration(
            DividerItemDecoration(
                this,
                (recyclerBuyTickets.layoutManager as LinearLayoutManager).orientation
            )
        )
        availableTicketsAdapter.onClickListener = createOnBookClickListener()

        observeCityPhoto()
        observeTickets()

        if (imageUrl.isNullOrEmpty()) {

            if (savedInstanceState == null) {
                ticketViewModel.fetchCityPhoto(city)
            }
        } else {
            loadCityImage(imageUrl)
        }

        if (savedInstanceState == null) {
            ticketViewModel.fetchTickets(city, iata)
        }
    }

    private fun observeCityPhoto() {
        ticketViewModel.cityPhoto().observe(this, Observer {
            it.url?.let { photoUrl -> loadCityImage(photoUrl) }
        })
    }

    private fun observeTickets() {
        ticketViewModel.tickets().observe(this, Observer {
            availableTicketsAdapter.submitList(it)
        })
    }

    private fun loadCityImage(imageUrl: String) {
        Picasso.get().load(imageUrl).fit().centerCrop().into(imageCityBackground)
    }

    private fun createOnBookClickListener(): AvailableTicketsAdapter.OnItemClickListener {
        return object : AvailableTicketsAdapter.OnItemClickListener {

            override fun onAvailableTicketClick(availableTicket: AvailableTicket) {
                ticketViewModel.addTicket(availableTicket)

                Toast.makeText(
                    this@TicketActivity,
                    getString(R.string.bought_ticket, availableTicket.toCity),
                    Toast.LENGTH_LONG
                ).show()

                val handler = Handler()
                handler.postDelayed({ finish() }, 1300)
            }
        }
    }

    companion object {
        private const val INTENT_CITY = "city"
        private const val INTENT_IATA = "iata"
        private const val INTENT_IMAGE_URL = "imageUrl"

        fun createIntent(
            context: Context,
            city: String,
            iata: String,
            imageUrl: String? = null
        ): Intent {
            return Intent(context, TicketActivity::class.java)
                .putExtra(INTENT_CITY, city)
                .putExtra(INTENT_IATA, iata)
                .putExtra(INTENT_IMAGE_URL, imageUrl)
        }
    }
}