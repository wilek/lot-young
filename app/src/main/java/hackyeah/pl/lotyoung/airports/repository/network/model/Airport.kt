package hackyeah.pl.lotyoung.airports.repository.network.model

data class Airport(val country: String, val cities: List<City>)