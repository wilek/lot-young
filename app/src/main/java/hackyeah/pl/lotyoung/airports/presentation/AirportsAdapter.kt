package hackyeah.pl.lotyoung.airports.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.airports.data.model.Airport

internal class AirportsAdapter(private val picasso: Picasso) :
    ListAdapter<Airport, AirportsAdapter.AiportViewHolder>(AirportDiff()) {

    var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AiportViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_airport, parent, false)
        val viewHolder = AiportViewHolder(view)
        view.setOnClickListener { onClickListener?.onAirportClick(getItem(viewHolder.adapterPosition)) }
        return viewHolder
    }

    override fun onBindViewHolder(holder: AiportViewHolder, position: Int) {
        val item = getItem(position)

        holder.textViewCity.text = item.city
        holder.textPriceStart.text =
            holder.itemView.context.resources.getString(R.string.price_from, item.promotionPrice)
        item.imageUrl?.let { imageUrl ->
            picasso.load(imageUrl).fit().centerCrop().into(holder.imageViewBackground)
        }
    }

    internal class AiportViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewBackground: ImageView = itemView.findViewById(R.id.imageCityBackground)
        val textViewCity: TextView = itemView.findViewById(R.id.textCityName)
        val textPriceStart: TextView = itemView.findViewById(R.id.textPriceStart)
    }

    internal class AirportDiff : DiffUtil.ItemCallback<Airport>() {
        override fun areItemsTheSame(oldItem: Airport, newItem: Airport): Boolean {
            return oldItem.iata == oldItem.iata
        }

        override fun areContentsTheSame(oldItem: Airport, newItem: Airport): Boolean {
            return oldItem == oldItem
        }
    }

    internal interface OnItemClickListener {
        fun onAirportClick(airport: Airport)
    }
}
