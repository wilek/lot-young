package hackyeah.pl.lotyoung.airports.presentation

import androidx.recyclerview.widget.GridLayoutManager

internal class AirportsSpanSizeLookup : GridLayoutManager.SpanSizeLookup() {

    override fun getSpanSize(position: Int): Int {
        return when (position % 6) {
            0 -> 6
            1, 2, 3 -> 2
            4 -> 3
            5 -> 3
            else -> 3
        }
    }
}