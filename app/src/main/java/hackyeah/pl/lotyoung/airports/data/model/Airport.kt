package hackyeah.pl.lotyoung.airports.data.model

data class Airport(val city: String, val iata: String, val imageUrl: String?, val promotionPrice: String)