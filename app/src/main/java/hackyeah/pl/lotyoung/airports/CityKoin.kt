package hackyeah.pl.lotyoung.airports

import hackyeah.pl.lotyoung.airports.data.network.CityImagesRepository
import hackyeah.pl.lotyoung.airports.data.network.LotNetworkRepository
import hackyeah.pl.lotyoung.airports.presentation.AirportsViewModel
import hackyeah.pl.lotyoung.network.AndroidSchedulerProvider
import hackyeah.pl.lotyoung.network.lot.LotNetworkClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val airportsKoinModule: Module = module {

    viewModel { AirportsViewModel(get(), get()) }

    factory { LotNetworkRepository(get(), get()) }

    single { LotNetworkClient().getLotApi() }

    single { AndroidSchedulerProvider() }

    single { CityImagesRepository() }
}