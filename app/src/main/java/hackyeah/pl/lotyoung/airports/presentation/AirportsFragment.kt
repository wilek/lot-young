package hackyeah.pl.lotyoung.airports.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.squareup.picasso.Picasso
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.airports.data.model.Airport
import hackyeah.pl.lotyoung.ticket.presentation.TicketActivity
import kotlinx.android.synthetic.main.fragment_flights.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AirportsFragment : Fragment() {

    private val airportsViewModel: AirportsViewModel by viewModel()
    private val adapter = AirportsAdapter(Picasso.get())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_flights, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = recyclerAirports.layoutManager as GridLayoutManager
        layoutManager.spanSizeLookup = AirportsSpanSizeLookup()
        recyclerAirports.adapter = adapter
        adapter.onClickListener = createOnAirportClickListener()

        observeFetchinAirports()

        if (savedInstanceState == null) {
            airportsViewModel.fetchAirports()
        }
    }

    private fun observeFetchinAirports() {
        airportsViewModel.airports().observe(this, Observer { airports ->
            adapter.submitList(airports)
        })
    }

    private fun createOnAirportClickListener(): AirportsAdapter.OnItemClickListener {
        return object : AirportsAdapter.OnItemClickListener {
            override fun onAirportClick(airport: Airport) {
                startActivity(
                    TicketActivity.createIntent(
                        requireContext(),
                        airport.city,
                        airport.iata,
                        null
                    )
                )
            }
        }
    }

    companion object {
        const val FRAGMENT_TAG = "discovery"
    }
}
