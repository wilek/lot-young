package hackyeah.pl.lotyoung.airports.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hackyeah.pl.lotyoung.airports.data.model.Airport
import hackyeah.pl.lotyoung.airports.data.network.CityImagesRepository
import hackyeah.pl.lotyoung.airports.data.network.LotNetworkRepository
import io.reactivex.disposables.CompositeDisposable

internal class AirportsViewModel(
    private val lotNetworkRepository: LotNetworkRepository,
    private val imagesRepository: CityImagesRepository
) : ViewModel() {

    private val disposables = CompositeDisposable()
    private val airports = MutableLiveData<List<Airport>>()

    fun airports(): LiveData<List<Airport>> = airports

    fun fetchAirports() {
        lotNetworkRepository.fetchAirports()
            .map { airports ->
                airports
                    .flatMap { airport -> airport.cities }
                    .map { airport ->
                        Airport(
                            airport.city,
                            airport.iata,
                            imagesRepository.getCityImageUrl(airport.iata),
                            (100..500).random().toString()
                        )
                    }
            }
            .subscribe({ airports.value = it },
                {
                    Log.d("ERROR", "" + it)
                    //TODO: Handle error
                }).let {
                disposables.add(it)
            }
    }

    /*private fun fetchCityImages(airport: List<Airport>) {
        Observable.just(airport)
            .flatMapIterable { it }
            .flatMap { airportData ->
                unsplashRepository.getCityPhoto(airportData.city, PhotoOrientation.SQUARISH, 1)
                    .map { airportData.copy(imageUrl = it.url) }
                    .toObservable()
            }
            .subscribe( { airportData ->
                val airportToUpdate = airports.value ?: emptyMap()
                val valueToUpdate = airportToUpdate[airportData.iata] ?: airportData
                airports.value = airportToUpdate + (airportData.iata to valueToUpdate)
            }, {
                Log.d("ERROR", "FETCHIN IMAGE ERROR" + it)
            })?.let {
                disposables.add(it)
            }
    }*/

    override fun onCleared() {
        disposables.clear()
    }
}