package hackyeah.pl.lotyoung.airports.data.network

import hackyeah.pl.lotyoung.airports.repository.network.model.Airport
import hackyeah.pl.lotyoung.network.AndroidSchedulerProvider
import hackyeah.pl.lotyoung.network.lot.LotApi
import io.reactivex.Single

internal class LotNetworkRepository(
    private val lotApi: LotApi,
    private val schedulersProvider: AndroidSchedulerProvider
) {
    fun fetchAirports(): Single<List<Airport>> {
        return lotApi.getAvailableFlights()
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.android())
    }
}