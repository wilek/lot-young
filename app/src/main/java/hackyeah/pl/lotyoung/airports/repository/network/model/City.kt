package hackyeah.pl.lotyoung.airports.repository.network.model

data class City(val city: String, val iata: String)