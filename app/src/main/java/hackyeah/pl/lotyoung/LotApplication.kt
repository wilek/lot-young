package hackyeah.pl.lotyoung

import android.app.Application
import com.facebook.stetho.Stetho
import hackyeah.pl.lotyoung.airports.airportsKoinModule
import hackyeah.pl.lotyoung.details.detailsKoinModule
import hackyeah.pl.lotyoung.ticket.ticketKoinModule
import hackyeah.pl.lotyoung.tickets.ticketsKoinModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class LotApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(this)
        startKoin { androidContext(this@LotApplication) }
        loadKoinModules(listOf(detailsKoinModule, airportsKoinModule, ticketKoinModule, ticketsKoinModule))
    }
}