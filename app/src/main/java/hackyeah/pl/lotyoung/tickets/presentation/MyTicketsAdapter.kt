package hackyeah.pl.lotyoung.tickets.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity

internal class MyTicketsAdapter : ListAdapter<TicketEntity, MyTicketsAdapter.MyTicketViewHolder>(MyTicketDiff()) {

    var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyTicketViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_my_ticket, parent, false)
        val viewHolder = MyTicketViewHolder(view)
        viewHolder.itemView.setOnClickListener {
            onClickListener?.onTicketClick(getItem(viewHolder.adapterPosition))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: MyTicketViewHolder, position: Int) {
        val item = getItem(position)

        holder.textDate.text = item.date
        holder.textFrom.text = item.fromCity + " (" + item.fromIata + ")"
        holder.textTo.text = item.toCity + " (" + item.toIata + ")"
    }

    internal class MyTicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textDate: TextView = itemView.findViewById(R.id.textDate)
        val textFrom: TextView = itemView.findViewById(R.id.textFrom)
        val textTo: TextView = itemView.findViewById(R.id.textTo)
    }

    internal class MyTicketDiff : DiffUtil.ItemCallback<TicketEntity>() {
        override fun areItemsTheSame(oldItem: TicketEntity, newItem: TicketEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TicketEntity, newItem: TicketEntity): Boolean {
            return oldItem == newItem
        }
    }

    internal interface OnItemClickListener {
        fun onTicketClick(ticket: TicketEntity)
    }
}
