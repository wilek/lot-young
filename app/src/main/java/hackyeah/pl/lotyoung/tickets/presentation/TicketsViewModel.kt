package hackyeah.pl.lotyoung.tickets.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hackyeah.pl.lotyoung.ticket.data.TicketsDatabaseRepository
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity
import io.reactivex.disposables.CompositeDisposable

internal class TicketsViewModel(private val databaseRepository: TicketsDatabaseRepository) :
    ViewModel() {

    private val disposables = CompositeDisposable()
    private val tickets = MutableLiveData<List<TicketEntity>>()

    fun tickets(): LiveData<List<TicketEntity>> = tickets

    fun fetchTickets() {
        databaseRepository.getTickets()
            .subscribe({
                tickets.value = it
            }, {
                // nop
            }).let {
                disposables.add(it)
            }
    }

}