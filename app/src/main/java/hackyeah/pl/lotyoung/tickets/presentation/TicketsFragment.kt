package hackyeah.pl.lotyoung.tickets.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import hackyeah.pl.lotyoung.R
import hackyeah.pl.lotyoung.details.TicketDetails
import hackyeah.pl.lotyoung.ticket.data.db.model.TicketEntity
import kotlinx.android.synthetic.main.fragment_tickets.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TicketsFragment: Fragment() {

    private val ticketsViewModel: TicketsViewModel by viewModel()
    private val myTicketsAdapter = MyTicketsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tickets, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerMyTickets.adapter = myTicketsAdapter
        recyclerMyTickets.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                (recyclerMyTickets.layoutManager as LinearLayoutManager).orientation
            )
        )
        myTicketsAdapter.onClickListener = createOnTicketClickListener()
        observeTickets()

        if (savedInstanceState == null) {
            ticketsViewModel.fetchTickets()
        }
    }

    private fun observeTickets() {
        ticketsViewModel.tickets().observe(this, Observer {
            myTicketsAdapter.submitList(it)
        })
    }

    private fun createOnTicketClickListener(): MyTicketsAdapter.OnItemClickListener {
        return object : MyTicketsAdapter.OnItemClickListener {

            override fun onTicketClick(ticket: TicketEntity) {
                startActivity(TicketDetails.createIntent(requireContext(), ticket))
            }
        }
    }

    companion object {
        const val FRAGMENT_TAG = "tickets"
    }
}