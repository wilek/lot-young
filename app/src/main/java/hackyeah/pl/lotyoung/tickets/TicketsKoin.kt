package hackyeah.pl.lotyoung.tickets

import hackyeah.pl.lotyoung.tickets.presentation.TicketsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val ticketsKoinModule: Module = module {

    viewModel { TicketsViewModel(get()) }
}