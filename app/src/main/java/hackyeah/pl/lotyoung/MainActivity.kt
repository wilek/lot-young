package hackyeah.pl.lotyoung

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import hackyeah.pl.lotyoung.airports.presentation.AirportsFragment
import hackyeah.pl.lotyoung.tickets.presentation.TicketsFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.toolbar

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.title = null

        bottomNavigation.setOnNavigationItemSelectedListener {
            if (it.itemId == R.id.action_discovert) {
                showFragment(AirportsFragment.FRAGMENT_TAG) { AirportsFragment() }
            } else {
                showFragment(TicketsFragment.FRAGMENT_TAG) { TicketsFragment() }
            }

            true
        }

        if (savedInstanceState == null) {
            showFragment(AirportsFragment.FRAGMENT_TAG) { AirportsFragment() }
        }
    }

    private fun showFragment(tag: String, createFragment: () -> Fragment) {
        supportFragmentManager.findFragmentByTag(tag)?.let {
            supportFragmentManager.beginTransaction().replace(R.id.container, it, tag).commit()
        } ?: run {
            supportFragmentManager.beginTransaction().replace(R.id.container, createFragment(), tag).commit()
        }
    }
}